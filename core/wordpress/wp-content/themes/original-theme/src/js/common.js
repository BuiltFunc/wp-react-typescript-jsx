/**
 * jQuery
 */
let $ = jQuery.noConflict();

/**
 * スマホ100vh対策
 */
let $_setViewportHeightVW;
let $_setViewportHeightVH;
function setViewportHeight() {
  let $_vw = window.innerWidth;
  let $_vh = window.innerHeight;
  let $_vmax = window.innerHeight;
  if ($_vw > $_vh) {
    $_vmax = $_vw * 0.01;
  } else {
    $_vmax = $_vh * 0.01;
  }
  if ($_setViewportHeightVH != $_vmax && $_setViewportHeightVW != $_vw) {
    $_setViewportHeightVH = $_vmax;
    $_setViewportHeightVW = $_vw
    document.documentElement.style.setProperty('--vmax', `${$_setViewportHeightVH}px`);
  }
}
setViewportHeight();

/**
 * アクセス直後
 */
$(document).ready(function () {
  // viewport
  (function ($) {
    let $_viewportFlag;
    $(window).on("resize orientationchange", function ($e) {
      let $_sw = window.screen.width;
      let $_timer = false;
      if ($_sw < 375) {
        //デバイス横幅375未満
        $("meta[name='viewport']").attr("content", "width=375px,user-scalable=0,shrink-to-fit=no");
      } else {
        //それ以外
        $("meta[name='viewport']").attr("content", "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,shrink-to-fit=no");
      }

      if ($_timer !== false) {
        clearTimeout($_timer);
      }
      $_timer = setTimeout(function () {
        setViewportHeight();
        $('body').trigger('touchmove');
        $(window).trigger('scroll');
      }, 100);
    }).trigger("resize");
  })(jQuery);

  // ready完了
  (function ($) {
    if (!$('body').hasClass('ready-complete')) {
      $('body').addClass('ready-complete');
    }
  })(jQuery);

  /**
   * mw_wp_form
   */
  (function ($) {
    if ($('.mw_wp_form').length) {
      $('.mw_wp_form form').attr('action', '#' + $('.mw_wp_form').attr('id'));
    }
  })(jQuery);

  /**
   * スマホグローバルナビ開閉ボタン
   */
  (function ($) {
    if ($('#js__globalNavButton').length) {
      $('#js__globalNavButton').on('click', function () {
        let $_self = $(this);
        let $_nav = $('.mainHeader__aside');
        if ($_nav.length) {
          $_nav.stop().slideToggle(200);
          $_nav.closest('#mainHeader').toggleClass('js__isOpen');
        }
      });
    }
  })(jQuery);
});

/**
 * 読み込み完了後
 */
$(window).on("load", function () {
  (function ($) {
    // load完了
    if (!$('body').hasClass('ready-complete')) {
      $('body').addClass('ready-complete');
    }
    if (!$('body').hasClass('load-complete')) {
      $('body').addClass('load-complete');
      $(this).trigger('loadCompleteAction');
    }
  })(jQuery);
});

/**
 * load完了後
 */
$(window).on("loadCompleteAction", function () {
  // 三点リーダ
  (function ($) {
    if ($('.js__max3row').length) {
      $('.js__max3row').each(function () {
        let $_target = $(this);
        let $_rest = "";
        let $_target_height = $_target.height();
        if ($_target.attr('data-max3row')) {
          $_target_height = parseFloat($_target.css('line-height')) * parseFloat($_target.attr('data-max3row'));
        }

        // オリジナルの文章を取得
        let $_html = $_target.html();

        // 対象の要素を、高さにautoを指定し非表示で複製する
        let $_clone = $_target.clone();
        $_clone
          .css({
            display: 'none',
            'max-height': 'none',
            'height': 'auto',
          })
          .width($_target.width());

        // 目印クラスをつけて
        // DOMを追加
        $_clone.addClass("max3row-rest");
        $_target.after($_clone);

        // 指定した高さになるまで、1文字ずつ消去していく
        while (($_html.length > 0) && ($_clone.height() > $_target_height)) {
          $_rest = $_html.substr($_html.length - 1, 1) + $_rest;
          $_html = $_html.substr(0, $_html.length - 1);
          $_clone.html($_html + '…'); // 高さ更新
        }

        // 文章差し替え
        if ($_rest == "") {
          $_target.html($_html);
        } else {
          $_target.html($_html + '店');
        }
        // リサイズ用に次の要素に残りの文章を入れておく
        $_clone.html($_rest);
      });
    }

    let $_timer = false;
    $(window).on('resize', function () {
      if ($('.js__max3row').length) {
        // タイマーによって、リサイズ単位毎に関数が実行され、重くなるのを防ぐ
        if ($_timer !== false) {
          clearTimeout($_timer);
        }
        $_timer = setTimeout(function () {
          $('.js__max3row').each(function () {

            let $_target = $(this);
            let $_rest;
            let $_target_height = $_target.height();
            if ($_target.attr('data-max3row')) {
              $_target_height = parseFloat($_target.css('line-height')) * parseFloat($_target.attr('data-max3row'));
            }

            // 以前にリサイズしたか(document.readyで必ず<p class="video-title-rest">
            // は存在するのでこの条件文はtrueを返すが、念のため)
            if ($_target.next().hasClass("max3row-rest")) {
              // 省略していた文章を取得
              $_rest = $_target.next().html();
              // 省略していた文章が空ではなかったとき、本文には３点リーダーが表示されて
              // いるので、その３文字を削除
              if ($_rest != "") {
                $_target.html($_target.html().slice(0, -3)); // 末尾の...を削除
              }
              // これがないと永遠に<p class="video-title-rest">が増えていく
              $_target.next().remove();
            } else {
              $_rest = "";
            }

            // オリジナルの文章を復元
            let $_html = $_target.html() + $_rest;

            // 対象の要素を、高さにautoを指定し非表示で複製する
            // 方針としては、このクローン(オリジナルの文章を保持)を非表示でブラウザに配置させ、
            // クローンの文字消去と元のボックスとの高さ比較を行うことによって、
            // クローンが元のボックスと同様の高さになったときの文章で差し替える
            let $_clone = $_target.clone();
            $_clone.html($_html);
            $_clone
              .css({
                display: 'none',
                'max-height': 'none',
                'height': 'auto',
              })
              .width($_target.width());

            // 目印クラスをつけて
            // DOMを追加 (これにより高さを獲得)
            $_clone.addClass("max3row-rest");
            $_target.after($_clone);

            $_rest = "";
            // 指定した高さになるまで、1文字ずつ消去していくと同時に、
            // 文章が完全消去されないように rest に退避させておく
            while (($_html.length > 0) && ($_clone.height() > $_target_height)) {
              $_rest = $_html.substr($_html.length - 1, 1) + $_rest;
              $_html = $_html.substr(0, $_html.length - 1);
              $_clone.html($_html + '…'); // 高さ更新
            }

            // 文章差し替え
            // rest が空っぽということは、三点リーダーを表示する必要がないということ
            if ($_rest == "") {
              $_target.html($_html);
            } else {
              $_target.html($_html + '…');
            }
            // 次のリサイズ用に次の要素に残りの文章を入れておく
            $_clone.html($_rest);
          });
        }, 200);
      }
    });
  })(jQuery);

  // アンカーリンク
  (function ($) {
    let $speed = 200;
    $(document).on('click', 'a[href^="#"]:not(.noscroll)', function ($e) {
      // アンカーの値取得width
      var $windowWidth = window.innerWidth ? window.innerWidth : $(window).width(),
        $windowHeight = window.innerHeight ? window.innerHeight : $(window).height(),
        $href = $(this).attr("href"),
        $hash = ($href ? '#' + $href.split('#')[1] : '');
      if ($href == '#' || ($('body').hasClass('front-page') && $(this).hasClass('pagetop'))) {
        // スムーススクロール
        $('html, body').stop().animate({
          scrollTop: 0
        }, $speed);
        return false;
      } else if ($hash && $($hash).length) {
        // 移動先を取得
        var $target = $($hash);
        // 移動先を数値で取得
        var $position = $target.offset().top - ($('#wpadminbar').length ? $('#wpadminbar').height() : 0);
        var $width = window.innerWidth ? window.innerWidth : $(window).width();
        /*if ($width <= 750) {
          $position -= $('#mainHeader').outerHeight();
        } else {
          $position -= $('#mainHeader').outerHeight();
        }*/
        // スムーススクロール
        $('html, body').stop().animate({
          scrollTop: $position
        }, $speed);
        return false;
      }
    });
  })(jQuery);

  /* 別ページからのアンカーリンク */
  (function ($) {
    let $urlHash = location.hash;
    if ($urlHash && $($urlHash).length) {
      let $_offset = $($urlHash).offset();
      $('html, body').scrollTop($_offset.top);
    }
  })(jQuery);

  // イベント発火
  $('body').trigger('touchmove');
  $(window).trigger('scroll');
  $(window).trigger('resize');
});
