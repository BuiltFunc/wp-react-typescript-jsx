/**
 * TOPページコンポーネントを定義
 */

import React from "react";
import { Init } from "../modules/wp-init";

export class Page extends React.Component {

  componentDidMount() {
    document.title = 'トップページ';
    if (document.getElementsByName('description')[0]) {
      document.getElementsByName('description')[0].setAttribute('content', 'トップページのディスクリプション');
    }
  }

  render() {
    return (
      <div>
        <div id="main">
        </div>
      </div>
    );
  }
}
