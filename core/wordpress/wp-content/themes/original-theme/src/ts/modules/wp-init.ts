/**
 * 初期設定
 */

export namespace Init {
  declare var $wp_data: any;
  export const $WP_DATA = (typeof $wp_data !== 'undefined') ? $wp_data : {};
}
