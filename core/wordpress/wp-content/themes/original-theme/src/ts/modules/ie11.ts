/**
 * IE11対策
 */

interface Element {
  msMatchesSelector(selectors: string): boolean;
}

if (!Element.prototype.matches) {
  Element.prototype.matches = Element.prototype.msMatchesSelector ||
    Element.prototype.webkitMatchesSelector;
}

if (!Element.prototype.closest) {
  Element.prototype.closest = function ($sting: any) {
    var $element: any = this;

    do {
      if (Element.prototype.matches.call($element, $sting)) return $element;
      $element = $element.parentElement || $element.parentNode;
    } while ($element !== null && $element.nodeType === 1);
    return null;
  };
}