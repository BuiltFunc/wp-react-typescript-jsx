/**
 * ヘッダコンポーネントを定義
 */

import React from "react";
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';
import { Init } from "../modules/wp-init";

export class MainHeader extends React.Component {
  render() {
    return (
      <Router>
        <header id="mainHeader" className="mainHeader">
          <div className="mainHeader__body">
            <h1 className="mainHeader__logo">
              <NavLink to='/' className="logo">
                <span>{Init.$WP_DATA.site_name}</span>
              </NavLink>
            </h1>
            <nav className="mainHeader__nav">
              <ul className="mainHeader__navMain">
                <li className="mainHeader__navMainItem">
                  <NavLink exact to='/' className="mainHeader__navMainItemLink" activeClassName="mainHeader__navMainItemLink--active">
                    <span className="material-icons-outlined">
                      home
                    </span>
                    <span>HOME</span>
                  </NavLink>
                </li>
                <li className="mainHeader__navMainItem">
                  <NavLink to='/about/' className="mainHeader__navMainItemLink" activeClassName="mainHeader__navMainItemLink--active">
                    <span className="material-icons-outlined">
                      face
                    </span>
                    <span>ABOUT US</span>
                  </NavLink>
                </li>
                <li className="mainHeader__navMainItem">
                  <NavLink to='/list/' className="mainHeader__navMainItemLink" activeClassName="mainHeader__navMainItemLink--active">
                    <span className="material-icons-outlined">
                      feed
                    </span>
                    <span>BLOG</span>
                  </NavLink>
                </li>
              </ul>
            </nav>
          </div>
        </header>
      </Router>
    );
  }
}

export default MainHeader;
