/**
 * フッターコンポーネントを定義
 */

import React from "react";
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';
import { Init } from "../modules/wp-init";

export class MainFooter extends React.Component {
  render() {
    return (
      <footer id="mainFooter" className="mainFooter">
        <div className="mainFooter__body">
          &copy; 2021 {Init.$WP_DATA.site_name}
        </div>
      </footer>
    )
  }
}
