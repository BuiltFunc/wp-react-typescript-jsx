/**
 * レイアウト
 */

import React from "react";
import { render } from "react-dom";
import { MainHeader } from "./components/mainHeader";
import { MainFooter } from "./components/mainFooter";
import { HooksUseBodyModule } from "./hooks/body";
import { functions } from "./modules/functions";
import { LoadModule } from "./modules/load";
const $loadModule = new LoadModule.Service();

interface LayoutState {
  title: string,
  discrption: string,
  isTouchDevice: boolean,
  isPc: boolean,
  isTablet: boolean,
  isSp: boolean,
  isScroll: number,
}

export default class Layout extends React.Component<{}, LayoutState> {
  constructor(props: never) {
    super(props);
    this.state = {
      title: '',
      discrption: '',
      isTouchDevice: functions.isTouchDevice(),
      isPc: functions.isPc(),
      isTablet: functions.isTablet(),
      isSp: functions.isSp(),
      isScroll: 0,
    };

    if (this.state.isPc) {

    }
  }

  /**
   * スクロール判定
   */
  public isScroll() {
    if (window.scrollY <= 100) {
      HooksUseBodyModule.addBodyClass('isScroll');
    } else {
      HooksUseBodyModule.removeBodyClass('isScroll');
    }
    this.setState({
      isScroll: window.scrollY,
    });
  }

  componentDidMount() {
    window.addEventListener('scroll', this.isScroll);
  }

  render() {
    return (
      <div className="app__body">
        <MainHeader />
        <main role="main" className="main">
          <article className="main__body">
          </article>
        </main>
        <MainFooter />
      </div>
    )
  }
};

const app = document.getElementById('app');
render(<Layout />, app);
