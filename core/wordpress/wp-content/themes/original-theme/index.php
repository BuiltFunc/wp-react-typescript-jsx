<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

get_header();
?>

<div id="app"></div>

<?php
get_footer();
