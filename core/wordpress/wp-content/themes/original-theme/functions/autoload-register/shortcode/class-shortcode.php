<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * ショートコード関連関連
 */
class Shortcode {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_filter( 'wp_kses_allowed_html', array( $this, 'wp_kses_allowed_html' ), 10, 2 );
		add_filter( 'init', array( $this, 'add_shortcode' ) );
	}

	/**
	 * ショートコード適用範囲
	 *
	 * @param array[]|string $context .
	 * @param string         $context_type .
	 */
	public function wp_kses_allowed_html( $context, $context_type ) {
		$context['img']['srcset'] = true;
		$context['source']['src'] = true;
		return $context;
	}

	/**
	 * ショートコード追加
	 */
	public function add_shortcode() {
		add_shortcode(
			'debug',
			function( $atts ) {
				$atts = shortcode_atts(
					array(
						'value' => '',
						'label' => '',
					),
					$atts,
					'get_template_part'
				);
				ob_start();
				debug( $atts['value'], $atts['label'] );
				$buff = ob_get_contents();
				ob_end_clean();
				return $buff;
			}
		);
	}
}

