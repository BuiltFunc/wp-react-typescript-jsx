<?php
/**
 * オリジナルテーマ
 *
 * Template Name: 準備中
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * 管理画面: 固定ページ一覧にテンプレート情報を表示
 */
class Admin_PostTypePage {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action(
			'load-edit.php',
			function() {
				$post_type = isset( $_REQUEST['post_type'] ) ? $_REQUEST['post_type'] : null;
				if ( ! empty( $post_type ) && 'page' === $post_type ) {
					add_filter( 'display_post_states', array( $this, 'display_post_states' ) );
				}
			}
		);
	}

	/**
	 * 固定ページ一覧にステータス表示を追加
	 *
	 * @param array $states .
	 */
	public function display_post_states( $states ) {
		global $post;
		$templates     = get_page_templates();
		$template_slug = get_page_template_slug( $post->ID );
		foreach ( $templates as $name => $file ) {
			if ( $file === $template_slug ) {
				$states[] = "{$name} ( tpl )";
			}
		}
		return $states;
	}
}

new Admin_PostTypePage();
