<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * セッション
 */
class Session {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action(
			'template_redirect',
			function() {
				if ( session_status() !== PHP_SESSION_ACTIVE ) {
					@session_start();
				}
			}
		);
	}
}

new Session();
