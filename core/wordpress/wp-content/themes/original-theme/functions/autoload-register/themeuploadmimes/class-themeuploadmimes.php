<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * ThemeUploadMimes
 */
class ThemeUploadMimes {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_filter( 'upload_mimes', array( $this, 'upload_mimes' ) );
	}

	/**
	 * Filter: upload_mimes
	 *
	 * @param array $mimes .
	 */
	public function upload_mimes( $mimes ) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}
}

new ThemeUploadMimes();
