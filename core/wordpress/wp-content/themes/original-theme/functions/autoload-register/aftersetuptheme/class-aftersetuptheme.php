<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * 初期設定: after_setup_theme
 */
class AfterSetupTheme {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
	}

	/**
	 * 初期設定: after_setup_theme
	 */
	public function after_setup_theme() {
		// titleタグの出力.
		add_theme_support( 'title-tag' );

		// HTML5のサポート.
		add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

		// WordPressのバージョン 削除.
		remove_action( 'wp_head', 'wp_generator' );

		// レティナパラメーター 削除.
		add_filter( 'wp_calculate_image_srcset_meta', '__return_null' );

		// 絵文字関連 削除.
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );

		// ブログ投稿ツールのためのタグ 削除.
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'wlwmanifest_link' );

		// rel=”prev”とrel=”next” 削除.
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );

		// Embedタグ 削除.
		remove_action( 'wp_head', 'rest_output_link_wp_head' );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );
	}
}

new AfterSetupTheme();
