<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * CSS JavaScript読み込み
 */
class Enqueue {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );
	}

	/**
	 * CSS JavaScript読み込み
	 */
	public function enqueue() {
		// CSSの読み込み.
		if ( ! is_admin() ) {
			$filepath = get_stylesheet_directory() . '/assets/css/style.min.css';
			if ( file_exists( $filepath ) ) {
				wp_enqueue_style(
					'theme-stylesheet',
					get_stylesheet_directory_uri() . '/assets/css/style.min.css',
					array(),
					filemtime(
						get_stylesheet_directory() . '/assets/css/style.min.css'
					)
				);
			}
		}

		// JavaScriptの読み込み.
		if ( ! is_admin() ) {
			/**
			 * TODO: コメントアウト
			wp_deregister_script( 'jquery' );
			wp_enqueue_script(
				'jquery',
				'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
				array(),
				'3.4.1',
				false
			);
			 */

			$filepath = get_stylesheet_directory() . '/assets/js/main.bundle.js';
			if ( file_exists( $filepath ) ) {
				/**
				 * 依存が必要になる場合は下記が必要となるかもしれない
				 * array( 'wp-element', 'wp-api-fetch' )
				 */
				wp_enqueue_script(
					'theme-common',
					get_stylesheet_directory_uri() . '/assets/js/main.bundle.js',
					array(),
					filemtime(
						get_stylesheet_directory() . '/assets/js/main.bundle.js'
					),
					true
				);

				wp_localize_script(
					'theme-common',
					'$wp_data',
					array(
						'home_url'                 => home_url(),
						'site_name'                => get_bloginfo( 'name' ),
						'stylesheet_directory_uri' => get_stylesheet_directory_uri(),
						'assets'                   => get_stylesheet_directory_uri() . '/assets',
					)
				);
			}
		}
	}
}

new Enqueue();
