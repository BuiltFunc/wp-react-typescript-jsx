<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * 必要なファイルを自動インクルードする
 */
require dirname( __FILE__ ) . '/functions.php';
theme_autoload_register( dirname( __FILE__ ) . '/autoload-register' );

/**
 * ACF
 */
require dirname( __FILE__ ) . '/themeacf/class-themeacf.php';

/**
 * カスタム投稿追加
 */
require dirname( __FILE__ ) . '/custompost/class-custompost.php';
