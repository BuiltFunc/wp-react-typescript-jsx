<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * Acfに依存する設定
 */
class ThemeAcf {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		if ( function_exists( 'acf_add_local_field_group' ) ) {
			require dirname( __FILE__ ) . '/acf-generate.php';
		}
		add_action( 'acf/init', array( $this, 'acf_add_options_page' ) );
	}

	/**
	 * オプションページを作成
	 */
	public function acf_add_options_page() {
		if ( function_exists( 'acf_add_options_page' ) ) {
			acf_add_options_page(
				array(
					'page_title' => 'テーマオプション',
					'menu_title' => 'テーマオプション',
					'menu_slug'  => 'original-theme-options-general',
					'capability' => 'edit_posts',
					'redirect'   => false,
					'icon_url'   => 'dashicons-admin-settings',
					'position'   => 3,
				)
			);

			acf_add_options_sub_page(
				array(
					'page_title'  => 'オプション設定',
					'menu_title'  => 'オプション設定',
					'parent_slug' => 'theme-general-settings',
				)
			);
		}
	}
}

add_action(
	'after_setup_theme',
	function() {
		if ( ! function_exists( 'get_field' ) ) {
			/**
			 * ACF get_fieldを作成しておく
			 *
			 * @param string $selector .
			 * @param mixed  $post_id .
			 * @param bool   $format_value .
			 */
			function get_field( $selector, $post_id = null, $format_value = true ) {
				return false;
			}
		}
	}
);

$GLOBALS['ThemeAcf'] = new ThemeAcf();
