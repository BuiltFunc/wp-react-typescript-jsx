<?php
/**
 * オリジナルテーマ
 *
 * @package WordPress
 * @subpackage original theme
 * @since original theme 1.0
 */

/**
 * カスタム投稿
 */
class CustomPost {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'register_post_types' ), PHP_INT_MAX );
	}

	/**
	 * カスタム投稿
	 */
	public function register_post_types() {
		$post_type_label = 'LP';
		$post_type       = 'lp';
		$post_type_slug  = 'lp';
		$labels          = array(
			'name'                  => $post_type_label,
			'singular_name'         => $post_type_label,
			'add_new'               => '新規追加',
			'add_new_item'          => "新規{$post_type_label}を追加",
			'edit_item'             => "{$post_type_label}の編集",
			'new_item'              => "新規{$post_type_label}",
			'view_item'             => "{$post_type_label}を表示",
			'view_items'            => "{$post_type_label}の表示",
			'search_items'          => "{$post_type_label}を検索",
			'not_found'             => "{$post_type_label}が見つかりませんでした。",
			'not_found_in_trash'    => "ゴミ箱内に{$post_type_label}が見つかりませんでした。",
			'parent_item_colon'     => '',
			'all_items'             => "{$post_type_label}一覧",
			'archives'              => "{$post_type_label}アーカイブ",
			'attributes'            => "{$post_type_label}の属性",
			'insert_into_item'      => "{$post_type_label}に挿入",
			'uploaded_to_this_item' => "この{$post_type_label}へのアップロード",
			'featured_image'        => 'アイキャッチ画像',
			'set_featured_image'    => 'アイキャッチ画像を設定',
			'remove_featured_image' => 'アイキャッチ画像を削除',
			'use_featured_image'    => 'アイキャッチ画像として使用',
			'filter_items_list'     => "{$post_type_label}リストの絞り込み",
			'items_list_navigation' => "{$post_type_label}リストナビゲーション",
			'items_list'            => "{$post_type_label}リスト",
			'menu_name'             => $post_type_label,
			'name_admin_bar'        => $post_type_label,
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => $post_type_slug ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
		);

		register_post_type( $post_type, $args );
	}
}

/**
 * TODO: コメントアウト
new CustomPost();
 */
