# リポジトリについて
WordPressテーマ開発  
作業時は、developで行ってください。  
react + typescript + jsx  
  

## クローン
適時変更してください。
```
$ git clone https://gitlab.com/BuiltFunc/wp-react-typescript-jsx.git .
```
  

## Dockerイメージのビルド

```
$ docker-compose build
```
  

## nodeパッケージインストール

```
$ npm install
```
  

## Dockerコンテナ起動

```
$ docker-compose up -d
```
  

## タスクの実行

```
$ npm run start
```
  

## Dockerコンテナに入る

```
$ docker-compose exec app /bin/bash
```

* タスクURL  
http://localhost:3000
    

# Dockerコンテナについて
* WordPressのコンテナ  
wordpress  
http://localhost

* MySQLのコンテナ  
mysql

* phpMyAdminのコンテナ  
phpmyadmin  
http://localhost:8080

* nginxのコンテナ  
nginx

* SMTPのコンテナ  
smtp:1025  
http://localhost:1080

# SMTPの使用方法
**MailCatcher** を使用して開発用簡易SMTPサーバにしています。

## 設定方法
WordPressインストール後 **WP Mail SMTP** というプラグインをインストールしてください。  
その後 **WP Mail SMTP** の設定画面より下記の設定してください。
* メーラー  
Other SMTP を選択

* SMTPホスト  
smtp と入力

* 暗号化  
None を選択

* SMTPポート  
1025 と入力

  

## テストメール
**Email Test** からテストメールを送ってください。
