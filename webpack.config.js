const path = require("path");
const TerserPlugin = require('terser-webpack-plugin');

module.exports = (env, argv) => {
  const IS_PRODUCTION = process.env.NODE_ENV == "production" ? true : false;

  return ({
    devtool: IS_PRODUCTION ? 'none' : 'source-map',

    watch: IS_PRODUCTION ? false : true,
    watchOptions: {
      ignored: /node_modules/
    },

    entry: {
      "main": path.resolve(__dirname, "./core/wordpress/wp-content/themes/original-theme/src/ts/App.tsx")
    },

    output: {
      path: path.resolve(__dirname, "./core/wordpress/wp-content/themes/original-theme/assets/js"),
      filename: "[name].bundle.js"
    },

    resolve: {
      extensions: [".ts", ".tsx", ".js"]
    },

    module: {
      rules: [
        {
          test: /\.(ts|tsx)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader?cacheDirectory',
            },
            {
              loader: 'ts-loader',
            },
          ],
        }
      ]
    },

    optimization: {
      minimize: IS_PRODUCTION === 'production' ? true : false,
      minimizer: IS_PRODUCTION === 'production' ?
        [new TerserPlugin({
          terserOptions: {
            compress: {
              drop_console: true,
            },
            output: {
              comments: false,
            },
          },
        })]
        : [],
    }
  });
};
